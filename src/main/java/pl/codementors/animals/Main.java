package pl.codementors.animals;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //1) utworzenie Scannera

        Scanner inputScanner = new Scanner(System.in);

        Animal[] animals = new Animal[1]; // tworze klatki dla zwierzat
        // umiesc zwierzaki w miejscach w klatce

        int i = 0;
        int repeats = animals.length;
        System.out.println();
        while (i < repeats) {

            System.out.println("please choose cage");
            int table = inputScanner.nextInt();
            System.out.println("please provide species: ");
            String species = inputScanner.next();

            if ("Cat".equals(species)) {
                animals[table] = new Cat();
            } else if ("Dog".equals(species)) {
                animals[table] = new Dog();
            } else if ("Wolf".equals(species)) {
                animals[table] = new Wolf();
            } else  if ("Ragdoll".equals(species)) {
                animals[table] = new Ragdoll();
            } else if ("Labrador".equals(species)) {
                animals[table] = new Labrador();
            }

            // pobranie nazwy zwierzatka

            System.out.println("Name of animal");
            String name = inputScanner.next();

            // ustawienie pobranej nazwy zwierza

            animals[table].setName(name);

            System.out.println(animals[table].getName());

            // wpisujemy wiek zwierza i tak jak wyzej
            System.out.println("Podaj wiek");
            int age = inputScanner.nextInt();
            animals[table].setAge(age);
            System.out.println(animals[table].getAge());

            System.out.print(i);
            System.out.print(" ");
            i = i + 1;
        }
        System.out.println("input cage number");
        int cage = inputScanner.nextInt();
        System.out.println(animals[cage].getClass().getSimpleName());
        System.out.println(animals[cage].getName());
        System.out.println(animals[cage].getAge());

        if (animals[cage] instanceof Dog) {
            Dog tempAnimal = (Dog) animals[cage];
            tempAnimal.hau();
        } else if (animals[cage] instanceof Cat) {
            Cat tempAnimal = (Cat) animals[cage];
            tempAnimal.miau();

        } else if (animals[cage] instanceof Wolf) {
            Wolf tempAnimal = (Wolf) animals[cage];
            tempAnimal.auuu();
        }
        System.out.println("glaszcze zwierze: ");
        if (animals[cage] instanceof Pet) {
            Pet tempHousepet = (Pet) animals[cage];
            tempHousepet.stroke();
        }
        else {
            System.out.println("leave me alone!"); }
        }
    }
