package pl.codementors.animals;

public class Cat extends Animal implements Pet {

    public Cat() {
    }

    public Cat(String name, int age) {
        super(name, age);
    }

    public void miau() {
        System.out.println("Miau!");
    }

    @Override
    public void stroke() {
        miau();
    }
}
