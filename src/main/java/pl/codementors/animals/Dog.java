package pl.codementors.animals;

public class Dog extends Animal implements Pet {

    public Dog() {
    }

    public Dog(String name, int age) {
        super(name, age);
    }
    public void hau() {
        System.out.println("Hau hau!");
    }

    @Override
    public void stroke() {
        hau();
    }
}
