package pl.codementors.animals;

public class Labrador extends Dog {
    public Labrador() {

    }

    @Override
    public void hau() {
        System.out.println("Woof woof");
    }

    @Override
    public void stroke() {
        hau();
    }
}