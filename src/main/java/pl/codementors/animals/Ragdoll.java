package pl.codementors.animals;

public class Ragdoll extends Cat {
    public Ragdoll() {

    }

    @Override
    public void miau() {
        System.out.println("Miauk");
    }

    @Override
    public void stroke() {
        miau();
    }
}